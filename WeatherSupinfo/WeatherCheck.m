//
//  WeatherCheck.m
//  WeatherSupinfo
//
//  Created by Mathieu on 6/25/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import "WeatherCheck.h"
#import "Alert.h"
#import "Result.h"
#import "Weather.h"

@implementation WeatherCheck
@synthesize alerts;

- (void) checkWeather{
    
    NSString *type = @"";
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    
    NSLog(@"checkWeather");
    
    self.alerts = [[NSMutableArray alloc] initWithArray:[Alert MR_findAll]];
    NSLog(@"%d", [self.alerts count]);

    for (Alert *alert in self.alerts) {
        
        if(![alert.a_Rain boolValue]){
            if(![alert.a_Cloud boolValue]){
                type = @"Clear";
            }else{
                type = @"Clouds";
            }
        }else{
            type = @"Rain";
        }
        
        NSLog(@"%@", [df stringFromDate:alert.a_TimeStart]);
        NSLog(@"%@", [df stringFromDate:[[NSDate alloc] init]]);
        NSLog(@"%@", type);
    
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:alert.a_City,@"q",nil];
        
        [[RKObjectManager sharedManager] getObjectsAtPath:@"weather" parameters:params success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            
            Result *result = (Result*)[mappingResult firstObject];
            
            if ([result.r_weather.w_main isEqualToString:type] && ([df stringFromDate:alert.a_TimeStart] >= [df stringFromDate:[[NSDate alloc] init]]) && ([df stringFromDate:alert.a_TimeEnd] <= [df stringFromDate:[[NSDate alloc] init]])) {
                
                NSLog(@"notification");
                NSDate *alertTime = [[NSDate date] dateByAddingTimeInterval:10];
                UIApplication *app = [UIApplication sharedApplication];
                UILocalNotification *notifyAlarm = [[UILocalNotification alloc] init];
                if(notifyAlarm){
                    notifyAlarm.fireDate = alertTime;
                    notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
                    notifyAlarm.repeatInterval = 0;
                    notifyAlarm.alertBody = [NSString stringWithFormat:@"%@ - %@ : %@", [df stringFromDate:alert.a_TimeStart], [df stringFromDate:alert.a_TimeEnd], type];
                    [app scheduleLocalNotification:notifyAlarm];
                }
                
            }
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NSLog(@"Hit error: %@", [error localizedDescription]);
            
        }];
    }
}

@end
