//
//  AlertsViewController.m
//  WeatherSupinfo
//
//  Created by Mathieu on 6/12/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import "AlertsViewController.h"
#import "AlertCell.h"
#import "Alert.h"
#import "DetailAlertViewController.h"

@interface AlertsViewController ()

@end

@implementation AlertsViewController
@synthesize  alerts, formatter;

#pragma mark - Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"HH:mm"];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.alerts = [[NSMutableArray alloc] initWithArray:[Alert MR_findAll]];
    [self.tv_Alerts reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTv_Alerts:nil];
    [self setBt_Edit:nil];
    [super viewDidUnload];
}


#pragma mark - TableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
        return [self.alerts count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *alertCellIdentifier = @"AlertCell";
    
    AlertCell *cell = [tableView dequeueReusableCellWithIdentifier:alertCellIdentifier];
    
    if(cell == nil){
        cell = (AlertCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:alertCellIdentifier];
    }

    
    [cell.iv_typeAlert setImage:[((Alert*)[self.alerts objectAtIndex:indexPath.row]) getImageForType]];
    cell.lb_cityAlert.text = ((Alert*)[self.alerts objectAtIndex:indexPath.row]).a_City;
    cell.lb_time.text = [formatter stringFromDate:((Alert*)[self.alerts objectAtIndex:indexPath.row]).a_TimeStart];
    cell.lb_time.text = [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:(((Alert*)[self.alerts objectAtIndex:indexPath.row]).a_TimeStart)], [formatter stringFromDate:(((Alert*)[self.alerts objectAtIndex:indexPath.row]).a_TimeEnd)]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}



#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [[self.alerts objectAtIndex:[indexPath row]] MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
        [self.alerts removeObjectAtIndex:[indexPath row]];
        [self.tv_Alerts reloadData];
    }
}

- (BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

- (void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
    NSString *mover = [self.alerts objectAtIndex:[sourceIndexPath row]];
    
    [self.alerts removeObjectAtIndex:[sourceIndexPath row]];
    [self.alerts insertObject:mover atIndex:[destinationIndexPath row]];
    
    [self.tv_Alerts setEditing:NO animated:YES];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"detailAlert"])
    {
        NSInteger selectedIndex = [[self.tv_Alerts indexPathForSelectedRow] row];
        DetailAlertViewController *davc = (DetailAlertViewController*)[segue destinationViewController];
        davc.alertSelected = [alerts objectAtIndex:selectedIndex];
    }
}


- (IBAction)editAlert:(id)sender {
    
    if(![self.tv_Alerts isEditing]){
        [self.tv_Alerts setEditing:YES animated:YES];
    }else{
        [self.tv_Alerts setEditing:NO animated:YES];
    }

}


@end
