//
//  Alert.m
//  WeatherSupinfo
//
//  Created by Mathieu on 6/15/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import "Alert.h"


@implementation Alert

@dynamic a_City;
@dynamic a_Cloud;
@dynamic a_Rain;
@dynamic a_Sun;
@dynamic a_MinTemp;
@dynamic a_MaxTemp;
@dynamic a_MinWind;
@dynamic a_MaxWind;
@dynamic a_TimeStart;
@dynamic a_TimeEnd;
@dynamic a_Forecast;

-(UIImage*)getImageForType{
    
    UIImage *typeImage = [[UIImage alloc] init];
    if(![[self a_Cloud] boolValue]){
        if(![[self a_Rain] boolValue]){
            if (![[self a_Sun] boolValue]) {
                //NSLog(@"default");
                typeImage = [UIImage imageNamed:@"Default.png"];
            }else{
                //NSLog(@"sun");
                typeImage = [UIImage imageNamed:@"sun.jpg"];
            }
        }else{
            //NSLog(@"rain");
            typeImage = [UIImage imageNamed:@"rain2.png"];
        }
    }else{
        //NSLog(@"cloud");
        typeImage = [UIImage imageNamed:@"cloud.jpg"];
    }
    
    return typeImage;
}

@end
