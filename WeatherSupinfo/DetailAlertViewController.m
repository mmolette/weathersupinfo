//
//  DetailAlertViewController.m
//  WeatherSupinfo
//
//  Created by Mathieu on 6/12/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import "DetailAlertViewController.h"
#import "UpdateAlertViewController.h"

@interface DetailAlertViewController ()

@end

@implementation DetailAlertViewController
@synthesize alertSelected, formatter;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"HH:mm"];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    [self.iv_type setImage:[self.alertSelected getImageForType]];
    self.lb_City.text = [self.alertSelected a_City];
    self.lb_ValueWhen.text = [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:[self.alertSelected a_TimeStart]], [formatter stringFromDate:[self.alertSelected a_TimeEnd]]];
    self.lb_ValueTemp.text = [NSString stringWithFormat:@"%@ to %@ C°", [self.alertSelected a_MinTemp], [self.alertSelected a_MaxTemp]];
    self.lb_ValueWind.text = [NSString stringWithFormat:@"%@ to %@ Km/h", [self.alertSelected a_MinWind], [self.alertSelected a_MaxWind]];
    self.lb_ValueForecast.text = [NSString stringWithFormat:@"%@", [self.alertSelected a_Forecast]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setIv_type:nil];
    [self setLb_Temp:nil];
    [self setLb_City:nil];
    [self setLb_ValueTemp:nil];
    [super viewDidUnload];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    

    if([[segue identifier] isEqualToString:@"UpdateAlert"])
    {
        UpdateAlertViewController *uavc = (UpdateAlertViewController*)[segue destinationViewController];
        uavc.alertSelected = self.alertSelected;
    }
}

@end
