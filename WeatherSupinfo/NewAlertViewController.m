//
//  NewAlertViewController.m
//  WeatherSupinfo
//
//  Created by Mathieu on 6/12/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import "NewAlertViewController.h"
#import "Alert.h"
#import "AlertsViewController.h"

@interface NewAlertViewController ()

@end

@implementation NewAlertViewController
@synthesize  cloud = _cloud;
@synthesize  rain = _rain;
@synthesize  sun = _sun;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.startDate = [[NSDate alloc] init];
    self.endDate = [[NSDate alloc] init];
    
    self.lb_MinTemp.text = [NSString stringWithFormat:@"Min. %0.f C°", roundf([self.s_MinTemp value])];
    self.lb_MaxTemp.text = [NSString stringWithFormat:@"Max. %0.f C°", roundf([self.s_MaxTemp value])];
    self.lb_MinWind.text = [NSString stringWithFormat:@"Wind min. %0.f (Km/h)", roundf([self.s_MinWind value])];
    self.lb_MaxWind.text = [NSString stringWithFormat:@"Wind max. %0.f (Km/h)", roundf([self.s_MaxWind value])];
    
    self.startTimeSelected = false;
    self.endTimeSelected = false;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    self.tf_StartTime.text = [NSString stringWithFormat:@"%@", [df stringFromDate:[[NSDate alloc] init]]];
    self.tf_EndTime.text = [NSString stringWithFormat:@"%@", [df stringFromDate:[[NSDate alloc] init]]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    self.tf_Forecast.inputAccessoryView = numberToolbar;
    
    self.cloud = false;
    self.rain = true;
    self.sun = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSv_newAlert:nil];
    [self setTf_city:nil];
    [self setSc_type:nil];
    [self setLb_StartTime:nil];
    [self setTf_StartTime:nil];
    [self setLb_EndTime:nil];
    [self setTf_EndTime:nil];
    [self setLb_Forecast:nil];
    [self setTf_Forecast:nil];
    [self setStepper_Forecast:nil];
    [super viewDidUnload];
}

#pragma mark - Actions

- (IBAction)sc_TypeChanged:(id)sender {
    NSLog(@"%d", self.sc_type.selectedSegmentIndex);
    switch (self.sc_type.selectedSegmentIndex) {
        case 0:
            self.cloud = false;
            self.rain = true;
            self.sun = false;
            break;
            
        case 1:
            self.cloud = true;
            self.rain = false;
            self.sun = false;
            break;
            
        case 2:
            self.cloud = false;
            self.rain = false;
            self.sun = true;
            break;
            
        default:
            break;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"saveAlert"])
    {
        Alert *alert = [Alert MR_createEntity];
        
        [alert setA_City:[self.tf_city text]];
        [alert setA_Cloud:[NSNumber numberWithBool:self.cloud]];
        [alert setA_Rain:[NSNumber numberWithBool:self.rain]];
        [alert setA_Sun:[NSNumber numberWithBool:self.sun]];
        [alert setA_MinTemp:[NSNumber numberWithFloat:roundf([self.s_MinTemp value])]];
        [alert setA_MaxTemp:[NSNumber numberWithFloat:roundf([self.s_MaxTemp value])]];
        [alert setA_MinWind:[NSNumber numberWithFloat:roundf([self.s_MinWind value])]];
        [alert setA_MaxWind:[NSNumber numberWithFloat:roundf([self.s_MaxWind value])]];
        [alert setA_TimeStart:self.startDate];
        [alert setA_TimeEnd:self.endDate];
        [alert setA_Forecast:[NSNumber numberWithInt:[[self.tf_Forecast text] intValue]]];
        
        [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
    }
}

- (IBAction)saveAlert:(id)sender {
    
    /*
    if(self.sun){
        NSLog(@"Sunny");
    }
    Alert *alert = [Alert MR_createEntity];
    
    [alert setA_City:[self.tf_city text]];
    [alert setA_Cloud:[NSNumber numberWithBool:self.cloud]];
    [alert setA_Rain:[NSNumber numberWithBool:self.rain]];
    [alert setA_Sun:[NSNumber numberWithBool:self.sun]];
    [alert setA_MinTemp:[NSNumber numberWithFloat:roundf([self.s_MinTemp value])]];
    [alert setA_MaxTemp:[NSNumber numberWithFloat:roundf([self.s_MaxTemp value])]];
    [alert setA_MinWind:[NSNumber numberWithFloat:roundf([self.s_MinWind value])]];
    [alert setA_MaxWind:[NSNumber numberWithFloat:roundf([self.s_MaxWind value])]];
    [alert setA_TimeStart:[[NSDate alloc] init]];
    [alert setA_TimeEnd:[[NSDate alloc] init]];
    [alert setA_Forecast:[NSNumber numberWithInt:[[self.tf_Forecast text] intValue]]];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
    [self.navigationController popViewControllerAnimated:YES];
     */
}

- (IBAction)minTempChanged:(id)sender {
    
    self.s_MaxTemp.minimumValue = self.s_MinTemp.value;
    //NSLog(@"test %0.f %0.f", self.s_MinTemp.value, self.s_MaxTemp.value);
    self.lb_MaxTemp.text = [NSString stringWithFormat:@"Max. %0.f C°", roundf([self.s_MaxTemp value])];
    self.lb_MinTemp.text = [NSString stringWithFormat:@"Min. %0.f C°", roundf([self.s_MinTemp value])];
}

- (IBAction)maxTempChanged:(id)sender {
    
    self.s_MinTemp.maximumValue = self.s_MaxTemp.value;
    self.lb_MinTemp.text = [NSString stringWithFormat:@"Min. %0.f C°", roundf([self.s_MinTemp value])];
    self.lb_MaxTemp.text = [NSString stringWithFormat:@"Max. %0.f C°", roundf([self.s_MaxTemp value])];
}

- (IBAction)minWindChanged:(id)sender {
    
    self.s_MaxWind.minimumValue = self.s_MinWind.value;
    self.lb_MaxWind.text = [NSString stringWithFormat:@"Wind max. %0.f (Km/h)", roundf([self.s_MaxWind value])];
    self.lb_MinWind.text = [NSString stringWithFormat:@"Wind min. %0.f (Km/h)", roundf([self.s_MinWind value])];
}

- (IBAction)maxWindChanged:(id)sender {
    
    self.s_MinWind.maximumValue = self.s_MaxWind.value;
    self.lb_MinWind.text = [NSString stringWithFormat:@"Wind min. %0.f (Km/h)", roundf([self.s_MinWind value])];
    self.lb_MaxWind.text = [NSString stringWithFormat:@"Wind max. %0.f (Km/h)", roundf([self.s_MaxWind value])];
}

- (IBAction)forecastChanged:(id)sender {
    
    self.stepper_Forecast.value = [self.tf_Forecast.text doubleValue];
}

- (IBAction)stepperChanged:(id)sender {
    
    self.tf_Forecast.text = [NSString stringWithFormat:@"%0.f", self.stepper_Forecast.value];
}


#pragma mark - TextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}


#pragma mark - NumberPad

-(void)cancelNumberPad{
    
    [self.tf_Forecast setText:@"0"];
    [self.stepper_Forecast setValue:0];
    [self.tf_Forecast resignFirstResponder];
}

-(void)doneWithNumberPad{
    
    [self.stepper_Forecast setValue:[self.tf_Forecast.text doubleValue]];
    [self.tf_Forecast resignFirstResponder];
}

- (IBAction)timeChanged:(id)sender {
}

- (IBAction)timeStartEditing:(id)sender {
    NSLog(@"tutu");
    [sender resignFirstResponder];
    [self.tp_time setHidden:FALSE];
    [self.tb_timepicker setHidden:FALSE];
    self.startTimeSelected = true;
}

- (IBAction)bt_doneBT:(id)sender {
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    if(self.startTimeSelected){
        self.startDate = self.tp_time.date;
        [df setDateFormat:@"HH:mm"];
        self.tf_StartTime.text = [NSString stringWithFormat:@"%@", [df stringFromDate:self.tp_time.date]];
    }else{
        self.endDate = self.tp_time.date;
        [df setDateFormat:@"HH:mm"];
        self.tf_EndTime.text = [NSString stringWithFormat:@"%@", [df stringFromDate:self.tp_time.date]];
    }
    [self.tb_timepicker setHidden:YES];
    [self.tp_time setHidden:YES];
    self.startTimeSelected = false;
    self.endTimeSelected = false;
}

- (IBAction)timeEndEditing:(id)sender {
    [sender resignFirstResponder];
    [self.tp_time setHidden:FALSE];
    [self.tb_timepicker setHidden:FALSE];
    self.endTimeSelected = true;
}

@end
