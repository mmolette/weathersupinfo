//
//  Coord.m
//  Weather Sample
//
//  Created by Thomas Leblond on 12/06/13.
//  Copyright (c) 2013 Thomas LEBLOND. All rights reserved.
//

#import "Coord.h"
#import "Result.h"


@implementation Coord

@synthesize c_lat, c_long;
@end
