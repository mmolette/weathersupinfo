//
//  Result.m
//  Weather Sample
//
//  Created by Thomas Leblond on 12/06/13.
//  Copyright (c) 2013 Thomas LEBLOND. All rights reserved.
//

#import "Result.h"
#import "Coord.h"
#import "Mainn.h"
#import "Sys.h"
#import "Weather.h"
#import "Wind.h"


@implementation Result

@synthesize r_weather, r_base, r_cod, r_coord, r_dt, r_id, r_main, r_name, r_sys, r_wind;
@end
