//
//  AlertsViewController.h
//  WeatherSupinfo
//
//  Created by Mathieu on 6/12/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    
    NSMutableArray *alerts;
    NSDateFormatter *formatter;
}

@property (retain, nonatomic) NSMutableArray *alerts;
@property (retain, nonatomic) NSMutableArray *filteredAlerts;
@property BOOL isSearching;
@property (retain, nonatomic) NSDateFormatter *formatter;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bt_Edit;


@property (weak, nonatomic) IBOutlet UITableView *tv_Alerts;

- (IBAction)editAlert:(id)sender;


@end
