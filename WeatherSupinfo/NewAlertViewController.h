//
//  NewAlertViewController.h
//  WeatherSupinfo
//
//  Created by Mathieu on 6/12/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewAlertViewController : UIViewController <UIScrollViewDelegate, UITextFieldDelegate>{
    BOOL cloud;
    BOOL rain;
    BOOL sun;
    BOOL startTimeSelected;
    BOOL endTimeSelected;
    NSDate *startDate;
    NSDate *endDate;
}

@property Boolean cloud;
@property Boolean rain;
@property Boolean sun;
@property Boolean startTimeSelected;
@property Boolean endTimeSelected;
@property (retain, nonatomic) NSDate *startDate;
@property (retain, nonatomic) NSDate *endDate;

@property (weak, nonatomic) IBOutlet UIScrollView *sv_newAlert;

@property (weak, nonatomic) IBOutlet UITextField *tf_city;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sc_type;


@property (weak, nonatomic) IBOutlet UILabel *lb_MinTemp;
@property (weak, nonatomic) IBOutlet UISlider *s_MinTemp;

@property (weak, nonatomic) IBOutlet UILabel *lb_MaxTemp;
@property (weak, nonatomic) IBOutlet UISlider *s_MaxTemp;

@property (weak, nonatomic) IBOutlet UILabel *lb_MinWind;
@property (weak, nonatomic) IBOutlet UISlider *s_MinWind;

@property (weak, nonatomic) IBOutlet UILabel *lb_MaxWind;
@property (weak, nonatomic) IBOutlet UISlider *s_MaxWind;

@property (weak, nonatomic) IBOutlet UILabel *lb_StartTime;
@property (weak, nonatomic) IBOutlet UITextField *tf_StartTime;

@property (weak, nonatomic) IBOutlet UILabel *lb_EndTime;
@property (weak, nonatomic) IBOutlet UITextField *tf_EndTime;

@property (weak, nonatomic) IBOutlet UILabel *lb_Forecast;
@property (weak, nonatomic) IBOutlet UITextField *tf_Forecast;
@property (weak, nonatomic) IBOutlet UIStepper *stepper_Forecast;

@property (weak, nonatomic) IBOutlet UIDatePicker *tp_time;
@property (weak, nonatomic) IBOutlet UIToolbar *tb_timepicker;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *tb_doneButton;

- (IBAction)sc_TypeChanged:(id)sender;
- (IBAction)saveAlert:(id)sender;

- (IBAction)minTempChanged:(id)sender;
- (IBAction)maxTempChanged:(id)sender;
- (IBAction)minWindChanged:(id)sender;
- (IBAction)maxWindChanged:(id)sender;

- (IBAction)forecastChanged:(id)sender;
- (IBAction)stepperChanged:(id)sender;

-(void)cancelNumberPad;
-(void)doneWithNumberPad;

- (IBAction)timeChanged:(id)sender;
- (IBAction)timeStartEditing:(id)sender;
- (IBAction)bt_doneBT:(id)sender;
- (IBAction)timeEndEditing:(id)sender;


@end
