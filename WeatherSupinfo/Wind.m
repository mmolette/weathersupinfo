//
//  Wind.m
//  Weather Sample
//
//  Created by Thomas Leblond on 12/06/13.
//  Copyright (c) 2013 Thomas LEBLOND. All rights reserved.
//

#import "Wind.h"
#import "Result.h"


@implementation Wind

@synthesize wi_deg, wi_gust, wi_speed;

@end
