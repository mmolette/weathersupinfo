//
//  WeatherCheck.h
//  WeatherSupinfo
//
//  Created by Mathieu on 6/25/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherCheck : NSObject {
    
    NSMutableArray *alerts;
}

@property (retain, nonatomic) NSMutableArray *alerts;

- (void) checkWeather;

@end
