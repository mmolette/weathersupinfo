//
//  UpdateAlertViewController.m
//  WeatherSupinfo
//
//  Created by Mathieu on 6/16/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import "UpdateAlertViewController.h"

@interface UpdateAlertViewController ()

@end

@implementation UpdateAlertViewController
@synthesize alertSelected;
@synthesize cloud = _cloud;
@synthesize rain = _rain;
@synthesize sun = _sun;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.startDate = [[NSDate alloc] init];
    self.endDate = [[NSDate alloc] init];
    
    self.cloud = [self.alertSelected.a_Cloud boolValue];
    self.rain = [self.alertSelected.a_Rain boolValue];
    self.sun = [self.alertSelected.a_Sun boolValue];
    
    if(!self.rain){
        if(!self.cloud){
            [self.sc_type setSelectedSegmentIndex:2];
        }else{
            [self.sc_type setSelectedSegmentIndex:1];
        }
    }else{
        [self.sc_type setSelectedSegmentIndex:0];
    }
    self.tf_city.text = self.alertSelected.a_City;
    
    self.s_MinTemp.value = [self.alertSelected.a_MinTemp floatValue];
    self.s_MaxTemp.value = [self.alertSelected.a_MaxTemp floatValue];
    self.s_MinWind.value = [self.alertSelected.a_MinWind floatValue];
    self.s_MaxWind.value = [self.alertSelected.a_MaxWind floatValue];
    self.lb_MinTemp.text = [NSString stringWithFormat:@"Min. %@ C°", alertSelected.a_MinTemp];
    self.lb_MaxTemp.text = [NSString stringWithFormat:@"Max. %@ C°", alertSelected.a_MaxTemp];
    self.lb_MinWind.text = [NSString stringWithFormat:@"Wind min. %@ (Km/h)", alertSelected.a_MinWind];
    self.lb_MaxWind.text = [NSString stringWithFormat:@"Wind max. %@ (Km/h)", alertSelected.a_MaxWind];
    self.startTimeSelected = false;
    self.endTimeSelected = false;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    
    self.tf_StartTime.text = [NSString stringWithFormat:@"%@", [df stringFromDate:alertSelected.a_TimeStart]];
    self.tf_EndTime.text = [NSString stringWithFormat:@"%@", [df stringFromDate:alertSelected.a_TimeEnd]];
    
    self.stepper_Forecast.value = [self.alertSelected.a_Forecast doubleValue];
    self.tf_Forecast.text = [NSString stringWithFormat:@"%@", self.alertSelected.a_Forecast];
     
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    self.tf_Forecast.inputAccessoryView = numberToolbar;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSv_newAlert:nil];
    [self setTf_city:nil];
    [self setSc_type:nil];
    [self setLb_StartTime:nil];
    [self setTf_StartTime:nil];
    [self setLb_EndTime:nil];
    [self setTf_EndTime:nil];
    [self setLb_Forecast:nil];
    [self setTf_Forecast:nil];
    [self setStepper_Forecast:nil];
    [self setTp_time:nil];
    [self setTb_timepicker:nil];
    [self setTb_doneButton:nil];
    [super viewDidUnload];
}

#pragma mark - Actions

- (IBAction)sc_TypeChanged:(id)sender {
    NSLog(@"%d", self.sc_type.selectedSegmentIndex);
    switch (self.sc_type.selectedSegmentIndex) {
        case 0:
            self.cloud = false;
            self.rain = true;
            self.sun = false;
            break;
            
        case 1:
            self.cloud = true;
            self.rain = false;
            self.sun = false;
            break;
            
        case 2:
            self.cloud = false;
            self.rain = false;
            self.sun = true;
            break;
            
        default:
            break;
    }
}


- (IBAction)minTempChanged:(id)sender {
    
    self.s_MaxTemp.minimumValue = self.s_MinTemp.value;
    //NSLog(@"test %0.f %0.f", self.s_MinTemp.value, self.s_MaxTemp.value);
    self.lb_MaxTemp.text = [NSString stringWithFormat:@"Max. %0.f C°", roundf([self.s_MaxTemp value])];
    self.lb_MinTemp.text = [NSString stringWithFormat:@"Min. %0.f C°", roundf([self.s_MinTemp value])];
}

- (IBAction)maxTempChanged:(id)sender {
    
    self.s_MinTemp.maximumValue = self.s_MaxTemp.value;
    self.lb_MinTemp.text = [NSString stringWithFormat:@"Min. %0.f C°", roundf([self.s_MinTemp value])];
    self.lb_MaxTemp.text = [NSString stringWithFormat:@"Max. %0.f C°", roundf([self.s_MaxTemp value])];
}

- (IBAction)minWindChanged:(id)sender {
    
    self.s_MaxWind.minimumValue = self.s_MinWind.value;
    self.lb_MaxWind.text = [NSString stringWithFormat:@"Wind max. %0.f (Km/h)", roundf([self.s_MaxWind value])];
    self.lb_MinWind.text = [NSString stringWithFormat:@"Wind min. %0.f (Km/h)", roundf([self.s_MinWind value])];
}

- (IBAction)maxWindChanged:(id)sender {
    
    self.s_MinWind.maximumValue = self.s_MaxWind.value;
    self.lb_MinWind.text = [NSString stringWithFormat:@"Wind min. %0.f (Km/h)", roundf([self.s_MinWind value])];
    self.lb_MaxWind.text = [NSString stringWithFormat:@"Wind max. %0.f (Km/h)", roundf([self.s_MaxWind value])];
}

- (IBAction)forecastChanged:(id)sender {
    
    self.stepper_Forecast.value = [self.tf_Forecast.text doubleValue];
}

- (IBAction)stepperChanged:(id)sender {
    
    self.tf_Forecast.text = [NSString stringWithFormat:@"%0.f", self.stepper_Forecast.value];
}


#pragma mark - TextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}


#pragma mark - NumberPad

-(void)cancelNumberPad{
    
    [self.tf_Forecast setText:@"0"];
    [self.stepper_Forecast setValue:0];
    [self.tf_Forecast resignFirstResponder];
}

-(void)doneWithNumberPad{
    
    [self.stepper_Forecast setValue:[self.tf_Forecast.text doubleValue]];
    [self.tf_Forecast resignFirstResponder];
}


- (IBAction)saveUpdateAlert:(id)sender {
    
    [self.alertSelected setA_City:[self.tf_city text]];
    [self.alertSelected setA_Cloud:[NSNumber numberWithBool:self.cloud]];
    [self.alertSelected setA_Rain:[NSNumber numberWithBool:self.rain]];
    [self.alertSelected setA_Sun:[NSNumber numberWithBool:self.sun]];
    [self.alertSelected setA_MinTemp:[NSNumber numberWithFloat:roundf([self.s_MinTemp value])]];
    [self.alertSelected setA_MaxTemp:[NSNumber numberWithFloat:roundf([self.s_MaxTemp value])]];
    [self.alertSelected setA_MinWind:[NSNumber numberWithFloat:roundf([self.s_MinWind value])]];
    [self.alertSelected setA_MaxWind:[NSNumber numberWithFloat:roundf([self.s_MaxWind value])]];
    [self.alertSelected setA_TimeStart:self.startDate];
    [self.alertSelected setA_TimeEnd:self.endDate];
    [self.alertSelected setA_Forecast:[NSNumber numberWithInt:[[self.tf_Forecast text] intValue]]];
        
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];

    

    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)timeChanged:(id)sender {
}

- (IBAction)timeStartEditing:(id)sender {
    [sender resignFirstResponder];
    [self.tp_time setHidden:FALSE];
    [self.tb_timepicker setHidden:FALSE];
    self.startTimeSelected = true;
}

- (IBAction)bt_doneBT:(id)sender {

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    if(self.startTimeSelected){
        self.startDate = self.tp_time.date;
        [df setDateFormat:@"HH:mm"];
        self.tf_StartTime.text = [NSString stringWithFormat:@"%@", [df stringFromDate:self.tp_time.date]];
    }
    if(self.endTimeSelected){
        self.endDate = self.tp_time.date;
        [df setDateFormat:@"HH:mm"];
        self.tf_EndTime.text = [NSString stringWithFormat:@"%@", [df stringFromDate:self.tp_time.date]];
    }
    [self.tb_timepicker setHidden:YES];
    [self.tp_time setHidden:YES];
    self.startTimeSelected = false;
    self.endTimeSelected = false;
}

- (IBAction)timeEndEditing:(id)sender {
    [sender resignFirstResponder];
    [self.tp_time setHidden:FALSE];
    [self.tb_timepicker setHidden:FALSE];
    self.endTimeSelected = true;
}

@end
