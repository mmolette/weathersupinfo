//
//  Sys.m
//  Weather Sample
//
//  Created by Thomas Leblond on 12/06/13.
//  Copyright (c) 2013 Thomas LEBLOND. All rights reserved.
//

#import "Sys.h"
#import "Result.h"


@implementation Sys

@synthesize s_country, s_sunrise, s_sunset;
@end
