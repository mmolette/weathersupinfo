//
//  Alert.h
//  WeatherSupinfo
//
//  Created by Mathieu on 6/15/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Alert : NSManagedObject

@property (nonatomic, retain) NSString * a_City;
@property (nonatomic, retain) NSNumber * a_Cloud;
@property (nonatomic, retain) NSNumber * a_Rain;
@property (nonatomic, retain) NSNumber * a_Sun;
@property (nonatomic, retain) NSNumber * a_MinTemp;
@property (nonatomic, retain) NSNumber * a_MaxTemp;
@property (nonatomic, retain) NSNumber * a_MinWind;
@property (nonatomic, retain) NSNumber * a_MaxWind;
@property (nonatomic, retain) NSDate * a_TimeStart;
@property (nonatomic, retain) NSDate * a_TimeEnd;
@property (nonatomic, retain) NSNumber * a_Forecast;

-(UIImage*) getImageForType;

@end
