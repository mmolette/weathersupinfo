//
//  DetailAlertViewController.h
//  WeatherSupinfo
//
//  Created by Mathieu on 6/12/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Alert.h"

@interface DetailAlertViewController : UIViewController{
    Alert *alertSelected;
    NSDateFormatter *formatter;
}

@property (retain, nonatomic) Alert *alertSelected;
@property (retain, nonatomic) NSDateFormatter *formatter;

@property (weak, nonatomic) IBOutlet UIImageView *iv_type;
@property (weak, nonatomic) IBOutlet UILabel *lb_City;
@property (weak, nonatomic) IBOutlet UILabel *lb_When;
@property (weak, nonatomic) IBOutlet UILabel *lb_Forecast;
@property (weak, nonatomic) IBOutlet UILabel *lb_ValueWhen;
@property (weak, nonatomic) IBOutlet UILabel *lb_ValueForecast;

@property (weak, nonatomic) IBOutlet UILabel *lb_Temp;
@property (weak, nonatomic) IBOutlet UILabel *lb_Wind;

@property (weak, nonatomic) IBOutlet UILabel *lb_ValueTemp;
@property (weak, nonatomic) IBOutlet UILabel *lb_ValueWind;


@end
