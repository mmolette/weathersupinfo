//
//  AlertCell.h
//  WeatherSupinfo
//
//  Created by Mathieu on 6/12/13.
//  Copyright (c) 2013 Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iv_typeAlert;
@property (weak, nonatomic) IBOutlet UILabel *lb_cityAlert;
@property (weak, nonatomic) IBOutlet UILabel *lb_time;


@end
