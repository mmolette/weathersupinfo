//
//  Mainn.m
//  Weather Sample
//
//  Created by Thomas Leblond on 12/06/13.
//  Copyright (c) 2013 Thomas LEBLOND. All rights reserved.
//

#import "Mainn.h"
#import "Result.h"


@implementation Mainn

@synthesize m_humidity, m_pressure, m_temp, m_temp_max, m_temp_min;
@end
