//
//  Weather.m
//  Weather Sample
//
//  Created by Thomas Leblond on 12/06/13.
//  Copyright (c) 2013 Thomas LEBLOND. All rights reserved.
//

#import "Weather.h"
#import "Result.h"


@implementation Weather

@synthesize w_main, w_description, w_icon, w_id;
@end
